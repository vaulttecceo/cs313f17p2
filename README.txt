TestIterator-
-line 77 - TODO what happens if you use list.remove(77)?
	>if you change the condition for removal you will also have to change the value to be removed

TestPerformance-
-Which of the two lists perfor better as the size increases?
	>depends on what the list is doing(see below)
	
	 size | performance(ms)
AL ACC     10 |	10
AL A/R	   10 |	28
LL ACC     10 |	11
LL A/R     10 |	22

AL ACC    100 |	11
AL A/R 	  100 |	55
LL ACC    100 |	25
LL A/R    100 |	21

AL ACC   1000 |	13
AL A/R   1000 |	383
LL ACC   1000 |	356
LL A/R   1000 |	21

AL ACC  10000 |	13
AL A/R  10000 |	3s 799ms
LL ACC  10000 |	4s 494ms
LL A/R  10000 |	23